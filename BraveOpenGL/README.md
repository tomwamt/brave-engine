# BraveOpenGL

The stock implementation of Graphics, Audio, Input, and UI plugins, using OpenTK. Forms the core of backend engine functionality.